//
//  MGScreenshotView.h
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MGAlignmentMode)
{
    MGAlignLeftEdges            = 1 << 0,
    MGAlignRightEdges           = 1 << 1,
    MGAlignHorizontalCenters    = 1 << 2,
    MGAlignTopEdges             = 1 << 3,
    MGAlignBottomEdges          = 1 << 4,
    MGAlignVerticalCenters      = 1 << 5,
};

@interface MGScreenshotView : UIImageView
@property (nonatomic, assign) CGFloat maskWidth;
@property (nonatomic, assign) CGFloat maskHeight;
@property (nonatomic, assign) MGAlignmentMode sourceViewAlignment;

- (instancetype)initWithSourceView:(UIView *)sourceView;

- (void)update;
@end
