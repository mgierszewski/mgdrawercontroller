//
//  MGScreenshotView.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGScreenshotView.h"
#import "MGDrawerController.h"

@interface MGScreenshotView ()
@property (nonatomic, weak) UIView *sourceView;
@end

@implementation MGScreenshotView

- (instancetype)initWithSourceView:(UIView *)sourceView
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        self.contentMode = UIViewContentModeScaleToFill;

#ifdef MGDrawerControllerUseAutolayout
        [super setTranslatesAutoresizingMaskIntoConstraints:NO];
#endif
        
        self.sourceView = sourceView;
        self.sourceViewAlignment = MGAlignHorizontalCenters | MGAlignVerticalCenters;
        self.maskHeight = 0.0f;
        self.maskHeight = 0.0f;
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)update
{
    CGSize maskSize = self.sourceView.frame.size;
    if (self.maskWidth > 0.0f)
    {
        maskSize.width = MIN(CGRectGetWidth(self.sourceView.frame), self.maskWidth);
    }
    if (self.maskHeight > 0.0f)
    {
        maskSize.height = MIN(CGRectGetHeight(self.sourceView.frame), self.maskHeight);
    }
    
    if (maskSize.width <= 0.0f || maskSize.height <= 0.0f)
    {
        return;
    }

    // align source
    CGPoint anchorPoint = CGPointZero;
    
    // align source horizontally
    if (self.sourceViewAlignment & MGAlignLeftEdges)
    {
        anchorPoint.x = self.sourceView.layer.cornerRadius;
    }
    else if (self.sourceViewAlignment & MGAlignRightEdges)
    {
        anchorPoint.x = CGRectGetWidth(self.sourceView.frame) - maskSize.width - self.sourceView.layer.cornerRadius;
    }
    else
    {
        anchorPoint.x = (CGRectGetWidth(self.sourceView.frame) - maskSize.width) / 2.0f;
    }

    // align source vertically
    if (self.sourceViewAlignment & MGAlignTopEdges)
    {
        anchorPoint.y = 0.0f;
    }
    else if (self.sourceViewAlignment & MGAlignBottomEdges)
    {
        anchorPoint.y = CGRectGetHeight(self.sourceView.frame) - maskSize.height - 0.0f;
    }
    else
    {
        anchorPoint.y = (CGRectGetHeight(self.sourceView.frame) - maskSize.height)/2.0f;
    }
    
    UIGraphicsBeginImageContextWithOptions(maskSize, NO, 0.0f);
    CGContextClipToRect(UIGraphicsGetCurrentContext(), (CGRect){CGPointZero, maskSize});
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -anchorPoint.x, -anchorPoint.y);
    
    [self.sourceView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.image = image;
}

@end
