//
//  UIColor+MGDrawerController.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "UIColor+MGDrawerController.h"

@implementation UIColor (MGDrawerController)

+ (UIColor *)interpolatedColorWithColor:(UIColor *)color1 andColor:(UIColor *)color2 interpolationFactor:(CGFloat)factor
{
    if (factor < 0.0f)
    {
        factor = 0.0f;
    }
    else if (factor > 1.0f)
    {
        factor = 1.0f;
    }
    
    CGFloat red1, green1, blue1, alpha1;
    CGFloat red2, green2, blue2, alpha2;
    
    [color1 getRed:&red1 green:&green1 blue:&blue1 alpha:&alpha1];
    [color2 getRed:&red2 green:&green2 blue:&blue2 alpha:&alpha2];
    
    return [UIColor colorWithRed:(red1 * factor + red2 * (1.0f - factor))
                           green:(green1 * factor + green2 * (1.0f - factor))
                            blue:(blue1 * factor + blue2 * (1.0f - factor))
                           alpha:(alpha1 * factor + alpha2 * (1.0f - factor))];
}

@end
