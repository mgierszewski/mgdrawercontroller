//
//  MGContainerView.h
//  MGPopoverController
//
//  Created by Maciek Gierszewski on 05.02.2015.
//  Copyright (c) 2015 Maciek Gierszewski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGContainerView : UIView
@property (nonatomic, weak, readonly) UIViewController *parentViewController;
@property (nonatomic, strong) UIViewController *contentViewController;

- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated;

- (instancetype)initWithParentViewController:(UIViewController *)viewController __attribute__((nonnull (1)));
+ (instancetype)containerViewWithParentViewController:(UIViewController *)viewController __attribute__((nonnull (1)));
@end
