//
//  MGDrawerController.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGDrawerController.h"
#import "MGContainerView.h"
#import "MGScreenshotView.h"
#import "UIColor+MGDrawerController.h"

#define DEFAULT_MENU_WIDTH (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 320.0f : 264.0f)

@interface MGDrawerController () <UIGestureRecognizerDelegate>

// navigation button
@property (nonatomic, strong) IBOutlet UIBarButtonItem *navigationButton;

// additional views
@property (nonatomic, strong) MGContainerView *menuView;
@property (nonatomic, strong) MGContainerView *contentView;
@property (nonatomic, strong) UIView *statusBarBackgroundView;
@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) MGScreenshotView *contentExtensionView;
@property (nonatomic, strong) MGScreenshotView *menuExtensionView;

// gesture recognizers
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

// constraints
#ifdef MGDrawerControllerUseAutolayout
@property (nonatomic, strong) NSArray *constraints;
#endif

// status bar colors for each drawer state
@property (nonatomic, strong) NSMutableDictionary *statusBarBackgroundColors;

// continuous drawer state (for interactive animation)
@property (nonatomic, assign) CGFloat continuousDrawerState;

@property (nonatomic, assign) CGFloat statusBarHeight;

// UI callbacks
- (void)navigationButtonTouchedUp:(id)sender;
- (void)panGestureRecognized:(UIPanGestureRecognizer *)panGestureRecognized;
- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognized;

@end

@implementation MGDrawerController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
		_statusBarHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
		_contentUnderStatusBar = NO;
        _drawerAlwaysOpen = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
        _continuousDrawerState = (CGFloat)_drawerAlwaysOpen;
        _drawerState = (MGDrawerState)_drawerAlwaysOpen;
        _drawerSide = MGDrawerSideLeft;
		_separatorColor = [UIColor lightGrayColor];
		_openDrawerContentCornerRadius = 4.0;
        
        self.statusBarBackgroundColors = [NSMutableDictionary new];
        self.statusBarBackgroundColors[@(MGDrawerStateOpen)] = [UIColor clearColor];
        self.statusBarBackgroundColors[@(MGDrawerStateClosed)] = [UIColor clearColor];
    }
    return self;
}

- (void)dealloc
{
    self.contentViewController = nil;
    self.menuViewController = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.navigationButton)
    {
        self.navigationButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:nil action:nil];
    }
    self.navigationButton.target = self;
    self.navigationButton.action = @selector(navigationButtonTouchedUp:);
    
    // status bar background view
    self.statusBarBackgroundView = [UIView new];
#ifdef MGDrawerControllerUseAutolayout
    self.statusBarBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
#endif
    
    // content container
    self.contentView = [[MGContainerView alloc] initWithParentViewController:self];
    
    self.contentExtensionView = [[MGScreenshotView alloc] initWithSourceView:self.contentView];
    self.contentExtensionView.maskWidth = 1.0f;
    self.contentExtensionView.sourceViewAlignment = ((self.drawerSide == MGDrawerSideLeft) ? MGAlignRightEdges : MGAlignLeftEdges) | MGAlignBottomEdges;

    self.menuView = [[MGContainerView alloc] initWithParentViewController:self];

    self.menuExtensionView = [[MGScreenshotView alloc] initWithSourceView:self.menuView];
    self.menuExtensionView.maskWidth = 1.0f;
    self.menuExtensionView.sourceViewAlignment = ((self.drawerSide == MGDrawerSideLeft) ? MGAlignRightEdges : MGAlignLeftEdges) | MGAlignBottomEdges;
	
	self.separatorView = [UIView new];
	self.separatorView.alpha = 0.0;
	self.separatorView.backgroundColor = self.separatorColor;
	
    // -- add subviews
    [self.view addSubview:self.menuExtensionView];
    [self.view addSubview:self.menuView];
    [self.view addSubview:self.contentExtensionView];
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.statusBarBackgroundView];
	
	[self.contentView addSubview:self.separatorView];
	
    // -- gesture recognizers
    self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)];
    self.panGestureRecognizer.delegate = self;
    self.panGestureRecognizer.cancelsTouchesInView = NO;
    [self.contentView addGestureRecognizer:self.panGestureRecognizer];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognized:)];
    self.tapGestureRecognizer.delegate = self;
    self.tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.contentView addGestureRecognizer:self.tapGestureRecognizer];
    
    // -- embed content
    @try
    {
        [self performSegueWithIdentifier:@"embedNavigation" sender:self];
    }
    @catch (NSException *exception)
    {
    }
    
    @try
    {
        [self performSegueWithIdentifier:@"embedMenu" sender:self];
    }
    @catch (NSException *exception)
    {
    }
    
    @try
    {
        [self performSegueWithIdentifier:@"embedContent" sender:self];
    }
    @catch (NSException *exception)
    {
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // hide the parent's navigation bar (if there is any)
    if (self.navigationController)
    {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
    if (self.isMovingToParentViewController || self.presentedViewController == nil)
    {
        // embed content
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if ([self.delegate respondsToSelector:@selector(statusBarStyleForDrawerState:)])
    {
        return [self.delegate statusBarStyleForDrawerState:(MGDrawerState)roundf(self.continuousDrawerState)];
    }
    
    return UIStatusBarStyleDefault;
}

#ifdef MGDrawerControllerUseAutolayout
- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
	_statusBarHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
	
    CGFloat inset = 0.0f;
    if (@available(iOS 11.0, *))
    {
        UIEdgeInsets insets = self.view.safeAreaInsets;
        inset = self.drawerSide == MGDrawerSideLeft ? insets.left : insets.right;
    }
    
    CGFloat menuViewWidth = DEFAULT_MENU_WIDTH + inset;
    
    // -- update constraints
    CGFloat contentViewOffset = self.continuousDrawerState * menuViewWidth;
    CGFloat contentViewWidth = CGRectGetWidth(self.view.bounds) - (self.drawerAlwaysOpen ? menuViewWidth : 0.0f);
    CGFloat menuExtensionWidth = CGRectGetWidth(self.view.bounds) - menuViewWidth;
    CGFloat contentExtensionWidth = CGRectGetWidth(self.view.bounds) - contentViewOffset;
	
    NSDictionary *views = @{ @"menuView": self.menuView,
                             @"statusBarBackgroundView": self.statusBarBackgroundView,
                             @"menuExtensionView": self.menuExtensionView,
                             @"contentExtensionView": self.contentExtensionView,
                             @"contentView": self.contentView,
                             };
    NSDictionary *metrics = @{ @"statusBarHeight": self.contentUnderStatusBar ? @0.0f : @(_statusBarHeight),
                               @"menuViewWidth": @(menuViewWidth),
                               @"contentViewWidth": @(contentViewWidth),
                               @"contentExtensionWidth": @(contentExtensionWidth),
                               @"menuExtensionWidth": @(menuExtensionWidth),
                               @"contentViewOffset": @(contentViewOffset)
							   };
    
    NSMutableArray *mutableConstraints = [NSMutableArray array];
    [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[statusBarBackgroundView]|" options:0 metrics:metrics views:views]];
    if (self.drawerSide == MGDrawerSideLeft)
    {
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[menuView(menuViewWidth)][menuExtensionView(menuExtensionWidth)]" options:0 metrics:metrics views:views]];
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-contentViewOffset-[contentView(contentViewWidth)]" options:0 metrics:metrics views:views]];
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-contentViewOffset-[contentExtensionView(contentExtensionWidth)]" options:0 metrics:metrics views:views]];
    }
    else
    {
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[menuExtensionView(menuExtensionWidth)][menuView(menuWidth)]|" options:0 metrics:metrics views:views]];
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[contentView(contentWidth)]-contentViewOffset-|" options:0 metrics:metrics views:views]];
        [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[contentExtensionView(contentExtensionWidth)]-contentViewOffset-|" options:0 metrics:metrics views:views]];
    }
    [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[menuView]|" options:0 metrics:metrics views:views]];
    [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[menuExtensionView]|" options:0 metrics:metrics views:views]];
    [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[statusBarBackgroundView(statusBarHeight)][contentView]|" options:0 metrics:metrics views:views]];
    [mutableConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[contentExtensionView(==contentView)]|" options:0 metrics:metrics views:views]];
    
    // - set constraints
    if (self.constraints)
    {
        [self.view removeConstraints:self.constraints];
    }
    self.constraints = [NSArray arrayWithArray:mutableConstraints];
    [self.view addConstraints:self.constraints];
}
#else 

- (void)viewWillLayoutSubviews
{
	_statusBarHeight = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
	if (self.contentUnderStatusBar)
	{
		_statusBarHeight = 0.0f;
	}
	
    CGFloat inset = 0.0f;
    if (@available(iOS 11.0, *))
    {
        UIEdgeInsets insets = self.view.safeAreaInsets;
        inset = self.drawerSide == MGDrawerSideLeft ? insets.left : insets.right;
    }
    
    CGFloat menuViewWidth = DEFAULT_MENU_WIDTH + inset;
    
    // -- update constraints
    CGFloat contentViewOffset = self.continuousDrawerState * menuViewWidth;
    CGFloat contentViewWidth = CGRectGetWidth(self.view.bounds) - (self.drawerAlwaysOpen ? menuViewWidth : 0.0f);
    CGFloat menuExtensionWidth = CGRectGetWidth(self.view.bounds) - menuViewWidth;
    CGFloat contentExtensionWidth = CGRectGetWidth(self.view.bounds) - contentViewOffset;
	CGFloat separatorWidth = 1.0f / [[UIScreen mainScreen] scale];
	
    CGRect contentViewFrame = self.contentView.frame;
    CGRect menuViewFrame = self.menuView.frame;
    CGRect statusBarBackgroundViewFrame = self.statusBarBackgroundView.frame;
    CGRect contentExtensionViewFrame = self.contentExtensionView.frame;
    CGRect menuExtensionViewFrame = self.menuExtensionView.frame;
	CGRect separatorViewFrame = self.separatorView.frame;
	
    statusBarBackgroundViewFrame.origin = CGPointMake(0.0f, 0.0f);
    statusBarBackgroundViewFrame.size = CGSizeMake(CGRectGetWidth(self.view.bounds), self.statusBarHeight);
    
    if (self.drawerSide == MGDrawerSideLeft)
    {
        menuViewFrame.origin = CGPointZero;
        menuViewFrame.size = CGSizeMake(menuViewWidth, CGRectGetHeight(self.view.bounds));
        
        menuExtensionViewFrame.origin = CGPointMake(CGRectGetMaxX(menuViewFrame), 0.0f);
        menuExtensionViewFrame.size = CGSizeMake(menuExtensionWidth, CGRectGetHeight(self.view.bounds));
        
		contentViewFrame.origin = CGPointMake(contentViewOffset, self.statusBarHeight);
        contentViewFrame.size = CGSizeMake(contentViewWidth, CGRectGetHeight(self.view.bounds) - self.statusBarHeight);
        
		contentExtensionViewFrame.origin = contentViewFrame.origin;
        contentExtensionViewFrame.size = CGSizeMake(contentExtensionWidth, self.statusBarHeight);
		
		separatorViewFrame.origin = CGPointMake(0.0, 0.0);
		separatorViewFrame.size = CGSizeMake(separatorWidth, CGRectGetHeight(contentViewFrame));
    }
    else
    {
        menuViewFrame.origin = CGPointMake(CGRectGetWidth(self.view.bounds) - menuViewWidth, 0.0f);
        menuViewFrame.size = CGSizeMake(menuViewWidth, CGRectGetHeight(self.view.bounds));
        
        menuExtensionViewFrame.origin = CGPointMake(0.0f, 0.0f);
        menuExtensionViewFrame.size = CGSizeMake(menuExtensionWidth, CGRectGetHeight(self.view.frame));
        
        contentViewFrame.origin = CGPointMake(CGRectGetWidth(self.view.bounds) - contentViewOffset, self.statusBarHeight);
        contentViewFrame.size = CGSizeMake(contentViewWidth, CGRectGetHeight(self.view.frame) - self.statusBarHeight);
        
		contentExtensionViewFrame.origin = contentViewFrame.origin;
        contentExtensionViewFrame.size = CGSizeMake(contentExtensionWidth, CGRectGetHeight(self.view.frame) - self.statusBarHeight);
		
		separatorViewFrame.origin = CGPointMake(CGRectGetWidth(contentViewFrame) - separatorWidth, 0.0);
		separatorViewFrame.size = CGSizeMake(separatorWidth, CGRectGetHeight(contentViewFrame));
    }

    self.contentView.frame = contentViewFrame;
    self.contentExtensionView.frame = contentExtensionViewFrame;
    self.menuView.frame = menuViewFrame;
    self.menuExtensionView.frame = menuExtensionViewFrame;
    self.statusBarBackgroundView.frame = statusBarBackgroundViewFrame;
	self.separatorView.frame = separatorViewFrame;
	
	[self.separatorView.superview bringSubviewToFront:self.separatorView];
}

#endif

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (self.drawerState == MGDrawerStateOpen)
    {
        [self.menuExtensionView update];
    }
}

#pragma mark - GestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIControl class]])
    {
        return NO;
    }
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.tapGestureRecognizer && self.drawerState == MGDrawerStateClosed)
    {
        return NO;
    }
    else if (gestureRecognizer == self.panGestureRecognizer && self.drawerAlwaysOpen)
    {
        return NO;
    }
    return YES;
}

#pragma mark - Navigation

- (void)setContinuousDrawerState:(CGFloat)continuousDrawerState
{
    if (self.drawerAlwaysOpen)
    {
        continuousDrawerState = (CGFloat)MGDrawerStateOpen;
    }
    _continuousDrawerState = continuousDrawerState;
    
    self.contentView.layer.cornerRadius = continuousDrawerState * self.openDrawerContentCornerRadius;
    self.contentExtensionView.layer.cornerRadius = continuousDrawerState * self.openDrawerContentCornerRadius;
	self.separatorView.alpha = continuousDrawerState;
    
    self.statusBarBackgroundView.backgroundColor = [UIColor interpolatedColorWithColor:self.statusBarBackgroundColors[@(MGDrawerStateOpen)]
                                                                              andColor:self.statusBarBackgroundColors[@(MGDrawerStateClosed)]
                                                                   interpolationFactor:continuousDrawerState];
    
#ifdef MGDrawerControllerUseAutolayout
    [self.view setNeedsUpdateConstraints];
    [self.view updateConstraintsIfNeeded];
#endif
    
    [self.view setNeedsLayout];
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

#pragma mark - Setters

- (void)setStatusBarHeight:(CGFloat)statusBarHeight
{
    _statusBarHeight = statusBarHeight;
    
#ifdef MGDrawerControllerUseAutolayout
    [self.view setNeedsUpdateConstraints];
#else
    [self.view setNeedsLayout];
#endif
}

- (void)setContentUnderStatusBar:(BOOL)contentUnderStatusBar
{
    _contentUnderStatusBar = contentUnderStatusBar;
    
#ifdef MGDrawerControllerUseAutolayout
    [self.view setNeedsUpdateConstraints];
#else
    [self.view setNeedsLayout];
#endif
}

- (void)setDrawerAlwaysOpen:(BOOL)drawerAlwaysOpen
{
    _drawerAlwaysOpen = drawerAlwaysOpen;
    
#ifdef MGDrawerControllerUseAutolayout
    [self.view setNeedsUpdateConstraints];
#else
    [self.view setNeedsLayout];
#endif
}

- (void)setSeparatorColor:(UIColor *)separatorColor
{
	_separatorColor = separatorColor;
	self.separatorView.backgroundColor = separatorColor;
}

- (void)setOpenDrawerContentCornerRadius:(CGFloat)openDrawerContentCornerRadius
{
	_openDrawerContentCornerRadius = openDrawerContentCornerRadius;
	self.continuousDrawerState = _continuousDrawerState;
}

#pragma mark - Status Bar Background Color

- (UIColor *)statusBarBackgroundColorForDrawerState:(MGDrawerState)state
{
    return self.statusBarBackgroundColors[@(state)];
}

- (void)setStatusBarBackgrounColor:(UIColor *)color forDrawerState:(MGDrawerState)state
{
    if (!color)
    {
        return;
    }
    
    self.statusBarBackgroundColors[@(state)] = color;
    self.statusBarBackgroundView.backgroundColor = [UIColor interpolatedColorWithColor:self.statusBarBackgroundColors[@(MGDrawerStateOpen)]
                                                                              andColor:self.statusBarBackgroundColors[@(MGDrawerStateClosed)]
                                                                   interpolationFactor:self.continuousDrawerState];
}

#pragma mark - UIGestureRecognizer

- (void)tapGestureRecognized:(UITapGestureRecognizer *)tapGestureRecognized
{
    if (tapGestureRecognized.state == UIGestureRecognizerStateRecognized)
    {
        [self setDrawerState:(1 - self.drawerState) animated:YES];
    }
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    if ([(UINavigationController *)self.contentView.contentViewController topViewController] != self.contentViewController)
    {
        return;
    }
    
    // obsluga gestu
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [sender translationInView:sender.view];
        [sender setTranslation:CGPointZero inView:sender.view];
        
        CGFloat progress = 0.0f;
        if (self.drawerSide == MGDrawerSideLeft)
        {
            progress = self.continuousDrawerState + (translation.x / CGRectGetWidth(self.menuView.frame));
        }
        else
        {
            progress = self.continuousDrawerState - (translation.x / CGRectGetWidth(self.menuView.frame));
        }
        self.continuousDrawerState = MIN(MAX(progress, 0.0f), 1.0f);
        
#ifndef MGDrawerControllerUseAutolayout
        [self.view layoutIfNeeded];
#endif
    }
    else
    {
        if (self.drawerState == MGDrawerStateClosed && self.continuousDrawerState > 0.2f)
        {
            [self setDrawerState:MGDrawerStateOpen animated:YES];
        }
        else if (self.drawerState == MGDrawerStateOpen && self.continuousDrawerState < 0.8f)
        {
            [self setDrawerState:MGDrawerStateClosed animated:YES];
        }
        else
        {
            [self setDrawerState:self.drawerState animated:YES];
        }
    }
}

#pragma mark - Show/Hide menu

- (void)setDrawerSide:(MGDrawerSide)drawerSide
{
    _drawerSide = drawerSide;
    
    self.contentExtensionView.sourceViewAlignment = ((self.drawerSide == MGDrawerSideLeft) ? MGAlignRightEdges : MGAlignLeftEdges) | MGAlignBottomEdges;
    self.menuExtensionView.sourceViewAlignment = ((self.drawerSide == MGDrawerSideLeft) ? MGAlignRightEdges : MGAlignLeftEdges) | MGAlignBottomEdges;

}

- (void)setDrawerState:(MGDrawerState)drawerState animated:(BOOL)animated
{
    if (drawerState == MGDrawerStateClosed && self.drawerAlwaysOpen)
    {
        return;
    }
    
    if (animated)
    {
        [self.contentExtensionView update];
    }
	
	if ([self.delegate respondsToSelector:@selector(drawerController:willChangeState:)])
	{
		[self.delegate drawerController:self willChangeState:drawerState];
	}
	
    _drawerState = drawerState;
    self.continuousDrawerState = (CGFloat)drawerState;
    
    void (^animationBlock)(void) = ^{
        [self.view layoutIfNeeded];
    };
    
    void (^completionBlock)(BOOL) = ^(BOOL flag){
        UINavigationController *navigationController = (UINavigationController *)self.contentView.contentViewController;
        navigationController.topViewController.view.userInteractionEnabled = (drawerState == MGDrawerStateClosed);
    };
    
    if (animated)
    {
        if ([UIView respondsToSelector:@selector(animateWithDuration:delay:usingSpringWithDamping:initialSpringVelocity:options:animations:completion:)] && drawerState != MGDrawerStateClosed)
        {
            [UIView animateWithDuration:0.5
                                  delay:0.0
                 usingSpringWithDamping:0.8f
                  initialSpringVelocity:15.0f
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:animationBlock
                             completion:completionBlock];
        }
        else
        {
            [UIView animateWithDuration:0.3
                                  delay:0.0
                                options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationCurveEaseInOut
                             animations:animationBlock
                             completion:completionBlock];
        }
    }
    else
    {
        animationBlock();
        completionBlock(YES);
    }
}

#pragma mark - Buttons

- (IBAction)navigationButtonTouchedUp:(id)sender
{
    [self setDrawerState:(1 - self.drawerState) animated:YES];
}

#pragma mark - Controller Setters

- (void)setContentNavigationController:(UINavigationController *)contentNavigationController
{
    if (self.contentNavigationController)
    {
        contentNavigationController.viewControllers = self.contentNavigationController.viewControllers;
    }
    
    _contentNavigationController = contentNavigationController;
    self.contentView.contentViewController = contentNavigationController;
}

- (void)setMenuViewController:(UIViewController *)menuViewController
{
    _menuViewController = menuViewController;
    self.menuView.contentViewController = menuViewController;
}

- (void)setContentViewController:(UIViewController *)contentViewController
{
    BOOL animated = self.drawerState == MGDrawerStateClosed && self.contentViewController != nil; //(self.drawerState == MGDrawerStateOpen && !self.drawerAlwaysOpen)
    [self setContentViewController:contentViewController animated:animated];
}

- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated
{
    _contentViewController = contentViewController;
    
    // insert into content view controller
    if (contentViewController)
    {
        if (!self.contentNavigationController)
        {
            self.contentNavigationController = [UINavigationController new];
        }
        NSArray *viewControllers = @[contentViewController];
        
        [self.contentNavigationController setDelegate:nil];
        [self.contentNavigationController setViewControllers:viewControllers animated:animated];
        
        // insert the navigation button
        if (!self.drawerAlwaysOpen)
        {
            if (self.drawerSide == MGDrawerSideLeft)
            {
                contentViewController.navigationItem.leftBarButtonItem = self.navigationButton;
            }
            else
            {
                contentViewController.navigationItem.rightBarButtonItem = self.navigationButton;
            }
        }
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(drawerController:preparesForSegue:sender:)])
    {
        [self.delegate drawerController:self preparesForSegue:segue sender:sender];
    }
}

@end
