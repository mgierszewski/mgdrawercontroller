//
//  MGNavigationEmbedSegue.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGNavigationEmbedSegue.h"
#import "MGDrawerController.h"

@implementation MGNavigationEmbedSegue

- (void)perform
{
    MGDrawerController *drawerController = (MGDrawerController *)self.sourceViewController;
    UIViewController *viewController = (UIViewController *)self.destinationViewController;

    if ([viewController isKindOfClass:[UINavigationController class]])
    {
        drawerController.contentNavigationController = (UINavigationController *)viewController;
    }
}

@end
