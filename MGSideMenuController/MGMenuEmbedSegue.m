//
//  MGDrawerEmbedSegue.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGMenuEmbedSegue.h"
#import "MGDrawerController.h"

@implementation MGMenuEmbedSegue

- (void)perform
{
    MGDrawerController *drawerController = (MGDrawerController *)self.sourceViewController;
    UIViewController *menuViewController = (UIViewController *)self.destinationViewController;
    
    drawerController.menuViewController = menuViewController;
}

@end
