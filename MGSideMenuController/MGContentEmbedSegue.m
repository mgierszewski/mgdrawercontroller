//
//  MGDrawerEmbedSegue.m
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGContentEmbedSegue.h"
#import "MGDrawerController.h"

@implementation MGContentEmbedSegue

- (void)perform
{
    MGDrawerController *drawerController = (MGDrawerController *)self.sourceViewController;
    UIViewController *topViewController = (UIViewController *)self.destinationViewController;
    
    drawerController.contentViewController = topViewController;
}

@end
