//
//  MGDrawerController.h
//  MGDrawerController
//
//  Created by Maciek Gierszewski on 08.10.2014.
//  Copyright (c) 2014 Maciek Gierszewski. All rights reserved.
//

#import "MGContentEmbedSegue.h"
#import "MGMenuEmbedSegue.h"
#import "MGNavigationEmbedSegue.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//#define MGDrawerControllerUseAutolayout

typedef NS_ENUM(NSUInteger, MGDrawerState)
{
    MGDrawerStateClosed,
    MGDrawerStateOpen,
};

typedef NS_ENUM(NSUInteger, MGDrawerSide)
{
    MGDrawerSideLeft,
    MGDrawerSideRight,
};

@class MGDrawerController;
@protocol MGDrawerControllerDelegate <NSObject>
@optional
- (void)drawerController:(MGDrawerController *)controller preparesForSegue:(UIStoryboardSegue *)segue sender:(id)sender;
- (UIStatusBarStyle)statusBarStyleForDrawerState:(MGDrawerState)state;
- (void)drawerController:(MGDrawerController *)controller willChangeState:(MGDrawerState)state;
@end

@interface MGDrawerController : UIViewController
@property (nonatomic, strong) UIViewController *menuViewController;                     // hidden controller
@property (nonatomic, strong) UIViewController *contentViewController;                  // visible controller
@property (nonatomic, strong) UINavigationController *contentNavigationController;      // navigation controller containing top view controller

@property (nonatomic, weak) IBOutlet id<MGDrawerControllerDelegate> delegate;

- (void)setContentViewController:(UIViewController *)contentViewController animated:(BOOL)animated;

// Drawer Side, Left is default
@property (nonatomic, assign) MGDrawerSide drawerSide;

// Drawer State
@property (nonatomic, assign) BOOL drawerAlwaysOpen;
@property (nonatomic, assign) MGDrawerState drawerState;
- (void)setDrawerState:(MGDrawerState)drawerState animated:(BOOL)animated;

// Status Bar Background Color
@property (nonatomic, assign) BOOL contentUnderStatusBar;
- (void)setStatusBarBackgrounColor:(UIColor *)color forDrawerState:(MGDrawerState)state;
- (UIColor *)statusBarBackgroundColorForDrawerState:(MGDrawerState)state;

// separator view
@property (nonatomic, strong) UIColor *separatorColor;
@property (nonatomic, assign) CGFloat openDrawerContentCornerRadius;

@end
